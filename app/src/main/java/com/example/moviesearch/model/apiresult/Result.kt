package com.example.moviesearch.model.apiresult

/**
 *  Class responsible for holding various data state while api & db calls
 *  MovieApiResponse = Generic Interface for holding any Kind of Data
 *  Any response which needs to be represented by this Class must implement MovieApiResponse
 *  FIXME: We can use Generics Instead of MovieApiResponse
 */

sealed class Result
class ResultSuccess(val resultCode: Int) : Result()
class ResultSuccessWithData(val resultCode: Int, val movieApiResponse: MovieApiResponse) : Result()
class ResultFailure(val resultCode: Int, val errorMsg: String) : Result()
