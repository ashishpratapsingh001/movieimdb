package com.example.moviesearch.model.apiresult;

/**
 * Any response which needs to be represented by this Class must implement MovieApiResponse
 */
public interface MovieApiResponse {
}
