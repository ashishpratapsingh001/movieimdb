package com.example.moviesearch.model.searchdetailpage

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.moviesearch.model.apiresult.MovieApiResponse
import com.google.gson.annotations.SerializedName

/**
 * Class that holds data for search &  detail api calls
 * This class also acts as Entity for our database
 * isFav ==>Additional field added for noting user favourites
 */
@Entity
data class SearchDetailItemResult(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("uniqueId") val uniqueId: Int,
    @SerializedName("isFav") var isFav: Boolean = false,
    @SerializedName("Released") val Released: String? = "",
    @SerializedName("imdbID") val imdbID: String? = "",
    @SerializedName("Rated") val Rated: String? = "",
    @SerializedName("imdbRating") val imdbRating: String = "",
    @SerializedName("Plot") val Plot: String? = "",
    @SerializedName("Title") val Title: String? = "",
    @SerializedName("Year") val Year: String? = "",
    @SerializedName("Poster") val Poster: String? = ""
) : MovieApiResponse


