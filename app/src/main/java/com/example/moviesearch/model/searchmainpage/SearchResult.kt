package com.example.moviesearch.model.searchmainpage

import com.example.moviesearch.model.apiresult.MovieApiResponse
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult
import com.google.gson.annotations.SerializedName

/**
 * Class that holds data for search &  detail api calls
 */
data class SearchResult(
    @SerializedName("Response") val Response: String? = "",
    @SerializedName("totalResults") val totalResults: String? = "",
    @SerializedName("Search") val Search: List<SearchDetailItemResult>?
) : MovieApiResponse