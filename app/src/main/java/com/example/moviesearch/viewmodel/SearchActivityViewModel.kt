package com.example.moviesearch.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesearch.R
import com.example.moviesearch.model.apiresult.Result
import com.example.moviesearch.model.apiresult.ResultFailure
import com.example.moviesearch.network.NetworkConstants
import com.example.moviesearch.repo.SearchActivityRepository
import com.example.moviesearch.util.UtilityClass
import kotlinx.coroutines.*

/**
 *  View model class for SearchActivity class
 */
class SearchActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val context by lazy { getApplication<Application>().applicationContext }
    private var repo: SearchActivityRepository
    private var job: Job? = null

    init {

        SearchActivityRepository().also { repo = it }
    }

    /**
     * If internet not there sending internet check msg to the user
     */
    fun getMovieListByName(keyword: String, pageNumber: String) {

        if (UtilityClass.isInternetAvailable(context)) {

            try {
                job?.cancel()
                job = viewModelScope.launch {

                    withContext(Dispatchers.IO) {
                        if (isActive)
                            repo.getMovieListByName(context, keyword, pageNumber)
                    }

                }
            } catch (e: Exception) {
            }
        } else {
            repo.movieApiResponseLiveData.postValue(
                ResultFailure(
                    NetworkConstants.CODE_400,
                    context.getString(R.string.check_internet_connection)
                )
            )
        }


    }

    fun getMovieResponseLivedata(): LiveData<Result> {
        return repo.movieApiResponseLiveData
    }
}