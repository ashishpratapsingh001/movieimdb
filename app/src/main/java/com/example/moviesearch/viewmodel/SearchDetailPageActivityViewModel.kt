package com.example.moviesearch.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesearch.model.apiresult.Result
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult
import com.example.moviesearch.repo.SearchDetailPageActivityRepo
import com.example.moviesearch.util.UtilityClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchDetailPageActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val context by lazy { getApplication<Application>().applicationContext }
    private var repo: SearchDetailPageActivityRepo
    private var mutableLiveDataOfDataList = MutableLiveData<List<SearchDetailItemResult>?>()

    init {

        SearchDetailPageActivityRepo().also { repo = it }
    }

    /**
     * If internet not there checing in db if fav is saved or not
     */
    fun getMovieDetailByImdbId(imdbId: String, isDataToBeFetchedFromDB: Boolean) {
        if (UtilityClass.isInternetAvailable(context) && !isDataToBeFetchedFromDB) {

            viewModelScope.launch {

                withContext(Dispatchers.IO) {
                    repo.getMovieDetailByImdbId(imdbId, context)

                }

            }
        } else {

            viewModelScope.launch {

                withContext(Dispatchers.IO) {
                    repo.getMovieDataFromDatabase(context, imdbId)

                }

            }

        }
    }

    fun getMovieDetailResponseLivedata(): LiveData<Result> {

        return repo.movieDetailApiResponseLiveData

    }

    fun saveMovieToDatabase(searchDetailItemResult: SearchDetailItemResult) {

        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                repo.saveMovieToDatabase(searchDetailItemResult, context)
            }
        }

    }

    fun deleteMovieFromDatabase(searchDetailItemResult: SearchDetailItemResult) {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                repo.deleteMovieFromDatabase(searchDetailItemResult, context)
            }
        }

    }

    fun getAllMoviesFromDatabase() {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                var list = repo.getAllMoviesFromDatabase(context)
                mutableLiveDataOfDataList.postValue(list)
            }
        }
    }

    fun getMutableLiveDataOfDataListFromDB(): LiveData<List<SearchDetailItemResult>?> {
        return mutableLiveDataOfDataList;
    }


}