package com.example.moviesearch.repo

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.moviesearch.R
import com.example.moviesearch.database.DatabaseBuilder
import com.example.moviesearch.model.apiresult.Result
import com.example.moviesearch.model.apiresult.ResultFailure
import com.example.moviesearch.model.apiresult.ResultSuccess
import com.example.moviesearch.model.apiresult.ResultSuccessWithData
import com.example.moviesearch.network.NetworkConstants
import com.example.moviesearch.network.RetrofitBuilderHelper
import com.example.myapplication.network.MovieSearchApiInterface

/**
 *  Repository class for SearchActivityViewModel class. It takes care of making api/db class on the
 *  basis of required criteria
 */
class SearchActivityRepository {

    var movieApiResponseLiveData = MutableLiveData<Result>()

    /**
     * Get list of movies by movie name
     * Pagination has been supported to avoid data payload in one go
     * If list is empty --> send resultwithoutdata obj
     * If list is non empty --> send resultdata obj
     * In case of error -> send resultFailure obj
     * === all are the objects of Result sealed class
     *
     */
    suspend fun getMovieListByName(context: Context, movieKeyword: String, pageNumber: String) {

        try {

            val apiInterface =
                RetrofitBuilderHelper.getRetrofitInstance()
                    ?.create(MovieSearchApiInterface::class.java)

            val movieApiResponse =
                apiInterface?.getMovieListByName(
                    NetworkConstants.TYPE_MOVIE,
                    movieKeyword,
                    NetworkConstants.APIKEY_VALUE,
                    pageNumber
                )
            if (movieApiResponse != null && movieApiResponse.isSuccessful) {
                val movieApiResponseBody = movieApiResponse.body()
                if (movieApiResponseBody != null && !movieApiResponseBody.totalResults.equals(
                        "0",
                        true
                    ) && !movieApiResponseBody.totalResults.isNullOrBlank() && !movieApiResponseBody.Response.equals(
                        "false",
                        true
                    ) && !movieApiResponseBody.Search!!.isNullOrEmpty()

                ) {

                    for (item in movieApiResponseBody.Search) {

                        /**
                         * Check if this Movie is a user fav movie is yes and mark isFav to true
                         * The above implementation will help us to identify the fav movie
                         */
                        val movieDao = DatabaseBuilder.getRoomDatabase(context)?.movieDao()

                        if (item.imdbID?.let { movieDao?.getMovieByIDFromDatabase(it) } != null) {
                            item.isFav = true
                        }

                    }
                    movieApiResponseLiveData.postValue(
                        ResultSuccessWithData(
                            200,
                            movieApiResponseBody
                        )
                    )

                } else {
                    movieApiResponseLiveData.postValue(ResultSuccess(NetworkConstants.CODE_200))
                }
            } else {
                movieApiResponseLiveData.postValue(
                    ResultFailure(
                        NetworkConstants.CODE_400,
                        context.getString(R.string.something_went_wrong_text)
                    )
                )
            }

        } catch (e: Exception) {
            //FIXME:Need to handle job cancellation
           /* movieApiResponseLiveData.postValue(
                ResultFailure(
                    NetworkConstants.CODE_400,
                    context.getString(R.string.fetching_details_failed)
                )
            )*/
        }

    }


}