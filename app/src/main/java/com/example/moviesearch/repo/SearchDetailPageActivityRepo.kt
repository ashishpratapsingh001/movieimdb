package com.example.moviesearch.repo

import android.content.Context
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.example.moviesearch.R
import com.example.moviesearch.database.DatabaseBuilder
import com.example.moviesearch.model.apiresult.Result
import com.example.moviesearch.model.apiresult.ResultFailure
import com.example.moviesearch.model.apiresult.ResultSuccess
import com.example.moviesearch.model.apiresult.ResultSuccessWithData
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult
import com.example.moviesearch.network.NetworkConstants
import com.example.moviesearch.network.RetrofitBuilderHelper
import com.example.myapplication.network.MovieSearchApiInterface

class SearchDetailPageActivityRepo {

    var movieDetailApiResponseLiveData = MutableLiveData<Result>()

    suspend fun getMovieDetailByImdbId(imdbId: String, context: Context) {

        try {

            val apiInterface =
                RetrofitBuilderHelper.getRetrofitInstance()
                    ?.create(MovieSearchApiInterface::class.java)

            val movieDetailApiResponse =
                apiInterface?.getMovieDetailByImdbId(NetworkConstants.APIKEY_VALUE, imdbId)
            if (movieDetailApiResponse != null && movieDetailApiResponse.isSuccessful) {
                val movieApiResponseBody = movieDetailApiResponse.body()
                if (movieApiResponseBody != null) {
                    movieDetailApiResponseLiveData.postValue(
                        ResultSuccessWithData(
                            NetworkConstants.CODE_200,
                            movieApiResponseBody
                        )
                    )

                } else {
                    movieDetailApiResponseLiveData.postValue(ResultSuccess(NetworkConstants.CODE_200))
                }
            } else {
                movieDetailApiResponseLiveData.postValue(
                    ResultFailure(
                        NetworkConstants.CODE_400,
                        context.getString(R.string.something_went_wrong_text)
                    )
                )
            }

        } catch (e: Exception) {
            movieDetailApiResponseLiveData.postValue(
                ResultFailure(
                    NetworkConstants.CODE_400,
                    context.getString(R.string.fetching_details_failed)
                )
            )
        }


    }

    suspend fun saveMovieToDatabase(
        searchDetailItemResult: SearchDetailItemResult,
        context: Context
    ) {
        val movieDao = DatabaseBuilder.getRoomDatabase(context)?.movieDao()
        var dataItem: SearchDetailItemResult? = null
        if (!TextUtils.isEmpty(searchDetailItemResult.imdbID)) {
            dataItem = movieDao?.getMovieByIDFromDatabase(searchDetailItemResult.imdbID!!)
        }
        if (dataItem == null) {
            movieDao?.saveMovieToDatabase(searchDetailItemResult)
        }


    }

    suspend fun deleteMovieFromDatabase(
        searchDetailItemResult: SearchDetailItemResult,
        context: Context
    ) {
        val movieDao = DatabaseBuilder.getRoomDatabase(context)?.movieDao()
        if (!TextUtils.isEmpty(searchDetailItemResult.imdbID)) {
            movieDao?.deleteMovieByIDFromDatabase(searchDetailItemResult.imdbID!!)

        }
    }

    suspend fun getAllMoviesFromDatabase(context: Context): List<SearchDetailItemResult>? {
        val movieDao = DatabaseBuilder.getRoomDatabase(context)?.movieDao()
        return movieDao?.getAllMoviesFromDatabase()
    }

    suspend fun getMovieDataFromDatabase(
        context: Context,
        imdbId: String
    ) {
        val movieDao = DatabaseBuilder.getRoomDatabase(context)?.movieDao()
        val dataItem = movieDao?.getMovieByIDFromDatabase(imdbId)
        if (dataItem != null) {
            movieDetailApiResponseLiveData.postValue(
                ResultSuccessWithData(
                    NetworkConstants.CODE_200,
                    dataItem
                )
            )
        } else {
            movieDetailApiResponseLiveData.postValue(
                ResultFailure(
                    NetworkConstants.CODE_400,
                    context.getString(R.string.fetching_details_failed)
                )
            )
        }
    }

}