package com.example.moviesearch.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult

/**
 *
 * DAO class for performing db operations
 *
 */
@Dao
interface MovieDao {

    /**
     * save movie to DB when user favourites it
     */
    @Insert
    suspend fun saveMovieToDatabase(searchDetailItemResultResponse: SearchDetailItemResult)

    /**
     * delete movie from DB when user un-favourites it
     */
    @Delete
    suspend fun deleteMovieFromDatabase(searchDetailItemResultResponse: SearchDetailItemResult): Int

    /**
     * Used for showing all list of movies stored in DB
     */
    @Query("Select * from SearchDetailItemResult")
    suspend fun getAllMoviesFromDatabase(): List<SearchDetailItemResult>

    /**
     * Get movie by its imdbId as it is unique for each movie
     */
    @Query("Select * from SearchDetailItemResult where imdbID =:imdbID")
    suspend fun getMovieByIDFromDatabase(imdbID: String): SearchDetailItemResult


    @Query("DELETE  from SearchDetailItemResult where imdbID =:imdbID")
    suspend fun deleteMovieByIDFromDatabase(imdbID: String): Int
}