package com.example.moviesearch.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult

/**
 * class responsible for creating entities and database version and provide DAO object
 */
@Database(entities = [SearchDetailItemResult::class], version = 1)
abstract class MovieAppDatabase :RoomDatabase(){
    abstract fun movieDao(): MovieDao
}