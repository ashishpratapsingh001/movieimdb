package com.example.moviesearch.database

import android.content.Context
import androidx.room.Room

/**
 *  singleton class responsible for creating roomDatabase object
 */
object DatabaseBuilder {

    private var roomDatabase: MovieAppDatabase? = null
    fun getRoomDatabase(applicationContext: Context): MovieAppDatabase? {
        if (roomDatabase == null) {
            roomDatabase = Room.databaseBuilder(
                applicationContext,
                MovieAppDatabase::class.java, "mydatabase"
            ).build()
        }

        return roomDatabase
    }


}