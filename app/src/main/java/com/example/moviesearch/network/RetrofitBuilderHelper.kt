package com.example.moviesearch.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton class for Retrofit instance builder
 */
object RetrofitBuilderHelper {

    private var retrofit: Retrofit? = null
    private const val BASE_URL = "http://www.omdbapi.com"


    fun getRetrofitInstance(): Retrofit? {

        if (retrofit == null) {

            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        }

        return retrofit

    }


}