package com.example.myapplication.network


import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult
import com.example.moviesearch.model.searchmainpage.SearchResult
import com.example.moviesearch.network.NetworkConstants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Api interface for  search &  detail api calls
 *
 */
interface MovieSearchApiInterface {

    @GET("/")
    suspend fun getMovieListByName(
        @Query(NetworkConstants.TYPE) type: String,
        @Query(NetworkConstants.KEYWORD) s: String,
        @Query(NetworkConstants.APIKEY) apikey: String,
        @Query(NetworkConstants.PAGE) page: String
    ): Response<SearchResult>

    @GET("/")
    suspend fun getMovieDetailByImdbId(
        @Query(NetworkConstants.APIKEY) apikey: String,
        @Query(NetworkConstants.IMDB) i: String
    ): Response<SearchDetailItemResult>
}