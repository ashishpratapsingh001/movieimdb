package com.example.moviesearch.network

class NetworkConstants {

    companion object {
        const val CODE_200 = 200
        const val CODE_400 = 200
        const val TYPE_MOVIE = "movie"
        const val TYPE = "type"
        const val KEYWORD = "s"
        const val APIKEY = "apikey"
        const val PAGE = "page"
        const val IMDB = "i"
        const val APIKEY_VALUE = "683fe327"

    }
}