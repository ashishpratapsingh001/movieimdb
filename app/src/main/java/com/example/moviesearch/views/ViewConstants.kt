package com.example.moviesearch.views

class ViewConstants {
    companion object {

        const val IMDB_ID = "imdbID"
        const val SCREEN_NAME = "screenName"
        const val IS_FAV = "isFav"
        const val FavouriteActivityScreen = "FavouriteActivity"
        const val POSITION = "position"
        const val REQUEST_CODE_2002 =2002
        const val REQUEST_CODE_2001 =2001
        const val REQUEST_CODE_2003 =2003
    }
}