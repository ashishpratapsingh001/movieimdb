package com.example.moviesearch.views

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesearch.R
import com.example.moviesearch.viewmodel.SearchDetailPageActivityViewModel
import com.example.moviesearch.views.ViewConstants.Companion.FavouriteActivityScreen
import com.example.moviesearch.views.ViewConstants.Companion.IMDB_ID
import com.example.moviesearch.views.ViewConstants.Companion.IS_FAV
import com.example.moviesearch.views.ViewConstants.Companion.POSITION
import com.example.moviesearch.views.ViewConstants.Companion.REQUEST_CODE_2002
import com.example.moviesearch.views.ViewConstants.Companion.SCREEN_NAME

class FavouriteActivity : AppCompatActivity(), onItemClickListener {
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var noData: TextView
    private lateinit var progressBar: ProgressBar
    private var misFav = false
    private var mPosition = 0;
    private var mImdbId = ""
    private lateinit var viewModel: SearchDetailPageActivityViewModel

    private val searchActivityAdapter by lazy {
        SearchActivityAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        initView()
    }

    override fun onResume() {
        super.onResume()
        initViewModel()
    }

    fun initView() {
        mRecyclerView = findViewById(R.id.fav_view_rv)
        noData = findViewById(R.id.no_data)
        progressBar = findViewById(R.id.progress_circular)
        mRecyclerView.adapter = searchActivityAdapter
        mRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    fun initViewModel() {

        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication())
        )[SearchDetailPageActivityViewModel::class.java]

        viewModel.getMutableLiveDataOfDataListFromDB().observe(this, Observer {

            if (it.isNullOrEmpty()) {
                noData.visibility = View.VISIBLE
                mRecyclerView.visibility = View.GONE
                progressBar.visibility = View.GONE
            } else {

                noData.visibility = View.GONE
                mRecyclerView.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                searchActivityAdapter.setAndNotifyData(it)
            }


        })
        noData.visibility = View.GONE
        mRecyclerView.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        viewModel.getAllMoviesFromDatabase()


    }

    override fun onItemClicked(position: Int, imdbID: String) {
        val intent = Intent(this, MovieDetailPageActivity::class.java)
        intent.putExtra(IMDB_ID, imdbID)
        intent.putExtra(SCREEN_NAME, FavouriteActivityScreen);
        intent.putExtra(IS_FAV, true)
        intent.putExtra(POSITION, position)
        mImdbId = imdbID
        startActivityForResult(intent, REQUEST_CODE_2002)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_2002) {
            if (resultCode == Activity.RESULT_OK) {
                val isFav = data?.getBooleanExtra(IS_FAV, false)
                val position = data?.getIntExtra(POSITION, 0)
                if (isFav != null && position != null) {
                    misFav = isFav
                    mPosition = position
                }
            }

        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(IS_FAV, misFav)
        intent.putExtra(POSITION, mPosition)
        intent.putExtra(IMDB_ID, mImdbId)
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }
}