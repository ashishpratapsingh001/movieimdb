package com.example.moviesearch.views

import androidx.recyclerview.widget.DiffUtil
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult

class MovieDiffUtil(
    private val oldUserList: List<SearchDetailItemResult>,
    private val newUserList: List<SearchDetailItemResult>
) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldUserList.size
    }

    override fun getNewListSize(): Int {
        return newUserList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

        return oldUserList[oldItemPosition].imdbID == newUserList[newItemPosition].imdbID

    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldUserList[oldItemPosition].Title == newUserList[newItemPosition].Title
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}