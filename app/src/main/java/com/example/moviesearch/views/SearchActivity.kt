package com.example.moviesearch.views

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesearch.R
import com.example.moviesearch.model.apiresult.ResultFailure
import com.example.moviesearch.model.apiresult.ResultSuccess
import com.example.moviesearch.model.apiresult.ResultSuccessWithData
import com.example.moviesearch.model.searchmainpage.SearchResult
import com.example.moviesearch.util.UtilityClass
import com.example.moviesearch.viewmodel.SearchActivityViewModel
import com.example.moviesearch.views.ViewConstants.Companion.IMDB_ID
import com.example.moviesearch.views.ViewConstants.Companion.IS_FAV
import com.example.moviesearch.views.ViewConstants.Companion.POSITION
import com.example.moviesearch.views.ViewConstants.Companion.REQUEST_CODE_2001
import com.example.moviesearch.views.ViewConstants.Companion.REQUEST_CODE_2003

class SearchActivity : AppCompatActivity(), onItemClickListener, View.OnClickListener {
    lateinit var mRecyclerView: RecyclerView
    lateinit var mSearchView: SearchView
    lateinit var mWelcomeUser: RelativeLayout
    private lateinit var viewModel: SearchActivityViewModel
    private lateinit var getMyFavImg: ImageView
    private lateinit var getMyFavText: TextView
    private var isLoading: Boolean = false
    private var searchKeyword: String = ""
    private var currentPageNumber = 1
    val searchActivityAdapter by lazy {
        SearchActivityAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initView()
        initViewModel()
        configureSearchView();


    }

    private fun configureSearchView() {
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                searchKeyword = newText
                viewModel.getMovieListByName(newText, "1")
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchKeyword = query
                viewModel.getMovieListByName(query, "1")
                return false
            }

        })
    }

    private fun initView() {
        mRecyclerView = findViewById(R.id.search_view_rv)
        mSearchView = findViewById(R.id.search_view)
        mWelcomeUser = findViewById(R.id.welcome_lay)
        getMyFavImg = findViewById(R.id.getMyFavImg)
        getMyFavImg.setOnClickListener(this)
        getMyFavText = findViewById(R.id.getMyFavTv)
        getMyFavText.setOnClickListener(this)
        mRecyclerView.adapter = searchActivityAdapter
        mRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!isLoading) {
                    //findLastCompletelyVisibleItemPostition() returns position of last fully visible view.
                    ////It checks, fully visible view is the last one.
                    if ((mRecyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() == searchActivityAdapter.itemCount - 1) {
                        loadMore()
                    }
                }

            }
        })

    }

    fun loadMore() {
        searchActivityAdapter.addLoader()
        currentPageNumber++
        isLoading = true
        viewModel.getMovieListByName(searchKeyword, currentPageNumber.toString())

    }

    fun initViewModel() {

        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        )[SearchActivityViewModel::class.java]

        viewModel.getMovieResponseLivedata().observe(this, Observer {
            if (currentPageNumber == 1) {
                mRecyclerView.visibility = View.VISIBLE
                mWelcomeUser.visibility = View.GONE
            }
            isLoading = false
            when (it) {

                is ResultSuccess -> {
                    println(it.resultCode)
                    if (currentPageNumber > 1) {
                        currentPageNumber--
                        searchActivityAdapter.removeLoader()
                    } else if (currentPageNumber == 1) {
                        searchActivityAdapter.clearAndNotify()
                    }

                }
                is ResultFailure -> {
                    println(it.resultCode)
                    Toast.makeText(
                        this,
                        getString(R.string.check_internet_connection),
                        Toast.LENGTH_SHORT
                    ).show()
                    if (currentPageNumber > 1) {
                        currentPageNumber--
                        searchActivityAdapter.removeLoader()
                    }
                }
                is ResultSuccessWithData -> {
                    if (it.movieApiResponse is SearchResult) {
                        println("${it.resultCode}   ${it.movieApiResponse.Response}")
                        if (currentPageNumber == 1)
                            searchActivityAdapter.setAndNotifyData(it.movieApiResponse.Search)
                        else {
                            searchActivityAdapter.removeLoader()
                            searchActivityAdapter.appendNewData(it.movieApiResponse.Search)
                        }
                    }
                }
            }


        })

    }

    override fun onItemClicked(position: Int, imdbID: String) {
        /*if (!UtilityClass.isInternetAvailable(this)) {
            Toast.makeText(this, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT)
                .show()
            return
        }*/
        val intent = Intent(this, MovieDetailPageActivity::class.java)
        intent.putExtra(IMDB_ID, imdbID)
        intent.putExtra(IS_FAV, searchActivityAdapter.isFav(position))
        intent.putExtra(POSITION, position)
        startActivityForResult(intent, REQUEST_CODE_2001)

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.getMyFavTv, R.id.getMyFavImg -> {
                val intent = Intent(this, FavouriteActivity::class.java)
                startActivityForResult(intent, REQUEST_CODE_2003)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == REQUEST_CODE_2001) {
            if (resultCode == Activity.RESULT_OK) {
                val isFav = data?.getBooleanExtra(IS_FAV, false)
                val position = data?.getIntExtra(POSITION, -1)
                if (isFav != null && position != null && position != -1) {
                    searchActivityAdapter.list[position].isFav = isFav
                    searchActivityAdapter.notifyItemChanged(position)
                }
            }

        } else if (requestCode == REQUEST_CODE_2003) {
            if (resultCode == Activity.RESULT_OK) {
                val isFav = data?.getBooleanExtra(IS_FAV, false)
                val imdbId = data?.getStringExtra(IMDB_ID)
                if (isFav != null && !TextUtils.isEmpty(imdbId)) {
                    var index = 0
                    for (item in searchActivityAdapter.list) {
                        if (imdbId.equals(item.imdbID)) {

                            searchActivityAdapter.list[index].isFav = isFav
                            searchActivityAdapter.notifyItemChanged(index)
                            break;
                        }
                        index++;
                    }

                }
            }

        }

        super.onActivityResult(requestCode, resultCode, data)
    }
}