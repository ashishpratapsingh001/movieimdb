package com.example.moviesearch.views

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moviesearch.R
import com.example.moviesearch.model.apiresult.ResultFailure
import com.example.moviesearch.model.apiresult.ResultSuccess
import com.example.moviesearch.model.apiresult.ResultSuccessWithData
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult
import com.example.moviesearch.viewmodel.SearchDetailPageActivityViewModel
import com.example.moviesearch.views.ViewConstants.Companion.FavouriteActivityScreen
import com.example.moviesearch.views.ViewConstants.Companion.IMDB_ID
import com.example.moviesearch.views.ViewConstants.Companion.IS_FAV
import com.example.moviesearch.views.ViewConstants.Companion.POSITION
import com.example.moviesearch.views.ViewConstants.Companion.SCREEN_NAME


class MovieDetailPageActivity : AppCompatActivity() {
    private lateinit var movieImg: ImageView
    private lateinit var movieTitle: TextView
    private lateinit var movieYear: TextView
    private lateinit var movieRating: RatingBar
    private lateinit var movieMPAARating: TextView
    private lateinit var movieFavIcon: ToggleButton
    private lateinit var movieStorylineHeading: TextView
    private lateinit var movieStoryline: TextView
    private lateinit var imgCardview: CardView
    private lateinit var progressBar: ProgressBar
    private lateinit var viewModel: SearchDetailPageActivityViewModel
    private var imdbId: String? = ""
    private var screenName: String? = ""
    private var isFav = false
    private var latestFavValue = false;
    private val valueNotAvailable = "N/A"
    private var searchDetailItemResult: SearchDetailItemResult? = null
    private var isDataToBeFetchedFromDB = false;
    private var itemPosition = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail_page)
        imdbId = intent.getStringExtra(IMDB_ID);
        isFav = intent.getBooleanExtra(IS_FAV, false)
        itemPosition = intent.getIntExtra(POSITION, 0)
        screenName = intent.getStringExtra(SCREEN_NAME)
        isDataToBeFetchedFromDB = FavouriteActivityScreen == screenName
        initViews()
        initViewModel()
    }

    private fun initViews() {
        movieImg = findViewById(R.id.movie_img)
        movieTitle = findViewById(R.id.movie_title)
        movieYear = findViewById(R.id.movie_year)
        movieRating = findViewById(R.id.movie_rating)
        movieMPAARating = findViewById(R.id.movie_mpaa_rating)
        movieFavIcon = findViewById(R.id.movie_fav_icon)
        movieStoryline = findViewById(R.id.movie_storyline)
        movieStorylineHeading = findViewById(R.id.movie_storyline_heading)
        imgCardview = findViewById(R.id.img_cardview)
        progressBar = findViewById(R.id.progress_circular)
        movieFavIcon.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                if (searchDetailItemResult != null) {
                    latestFavValue = true
                    searchDetailItemResult!!.isFav = true
                    viewModel.saveMovieToDatabase(searchDetailItemResult!!)
                }

            } else {
                if (searchDetailItemResult != null) {
                    latestFavValue = false
                    searchDetailItemResult!!.isFav = false
                    viewModel.deleteMovieFromDatabase(searchDetailItemResult!!)
                }
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        )[SearchDetailPageActivityViewModel::class.java]


        viewModel.getMovieDetailResponseLivedata().observe(this, Observer {
            progressBar.visibility = View.GONE
            when (it) {

                is ResultSuccess -> {
                    println(it.resultCode)

                }
                is ResultFailure -> {
                    println(it.resultCode)
                    Toast.makeText(
                        this,
                        getString(R.string.check_internet_connection),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is ResultSuccessWithData -> {
                    if (it.movieApiResponse is SearchDetailItemResult) {
                        this.searchDetailItemResult = it.movieApiResponse
                        updateView(it.movieApiResponse)
                    }
                }
            }


        })


        if (!TextUtils.isEmpty(imdbId)) {
            viewModel.getMovieDetailByImdbId(imdbId!!, isDataToBeFetchedFromDB);
        } else {
            Toast.makeText(this, getString(R.string.movie_not_available), Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateView(movieApiResponse: SearchDetailItemResult) {

        val requestOptions = RequestOptions();
        requestOptions.placeholder(R.drawable.ic_baseline_movie_24);

        imgCardview.visibility = View.VISIBLE
        movieFavIcon.visibility = View.VISIBLE
        Glide.with(this)
            .setDefaultRequestOptions(requestOptions)
            .load(movieApiResponse.Poster).into(movieImg)

        if (!TextUtils.isEmpty(movieApiResponse.Title) && !valueNotAvailable.equals(
                movieApiResponse.Title,
                true
            )
        ) {
            movieTitle.text = movieApiResponse.Title
            movieTitle.visibility = View.VISIBLE
        }
        if (!TextUtils.isEmpty(movieApiResponse.Released) && !valueNotAvailable.equals(
                movieApiResponse.Released,
                true
            )
        ) {
            movieYear.text = movieApiResponse.Released
            movieYear.visibility = View.VISIBLE
        }
        if (!TextUtils.isEmpty(movieApiResponse.Rated) && !valueNotAvailable.equals(
                movieApiResponse.Rated,
                true
            )
        ) {
            movieMPAARating.text = movieApiResponse.Rated
            movieMPAARating.visibility = View.VISIBLE
        }
        if (!TextUtils.isEmpty(movieApiResponse.Plot) && !valueNotAvailable.equals(
                movieApiResponse.Plot,
                true
            )
        ) {
            movieStoryline.text = movieApiResponse.Plot
            movieStoryline.visibility = View.VISIBLE
            movieStorylineHeading.visibility = View.VISIBLE
        } else {
            movieStorylineHeading.visibility = View.GONE
        }
        if (!TextUtils.isEmpty(movieApiResponse.imdbRating) && !valueNotAvailable.equals(
                movieApiResponse.imdbRating,
                true
            )
        ) {
            try {
                var value = movieApiResponse.imdbRating.toFloat() / 10 * 5
                movieRating.visibility = View.VISIBLE
                movieRating.rating = value
            } catch (e: Exception) {
                movieRating.visibility = View.GONE
            }
        }
        movieFavIcon.isChecked = isFav

    }


    override fun onBackPressed() {
        val intent = Intent()
        if (isFav != latestFavValue) {
            intent.putExtra(IS_FAV, latestFavValue)
            intent.putExtra(POSITION, itemPosition)

        }
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }

}