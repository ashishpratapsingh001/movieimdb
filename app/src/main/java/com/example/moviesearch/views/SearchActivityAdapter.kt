package com.example.moviesearch.views

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moviesearch.R
import com.example.moviesearch.model.searchdetailpage.SearchDetailItemResult


class SearchActivityAdapter(private val onItemClickListener: onItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var list = ArrayList<SearchDetailItemResult>()

    companion object {
        const val INFLATE_SEARCH_ITEM = 1
        const val INFLATE_LOADER_ITEM = 2
        const val LOADER_IMDB_ID = "Loader_IMDB_ID"
    }

    override fun getItemViewType(position: Int): Int {
        if (!LOADER_IMDB_ID.equals(list[position].imdbID, false)) {
            return INFLATE_SEARCH_ITEM
        }
        return INFLATE_LOADER_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == INFLATE_SEARCH_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_search_item, parent, false)

            return MovieViewHolder(view, onItemClickListener)
        }

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_search_loader, parent, false)

        return ProgressViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is MovieViewHolder) {
            holder.bindUI(position, list)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setAndNotifyData(data: List<SearchDetailItemResult>?) {

        if (data != null) {

            val diffUtil = MovieDiffUtil(list, data)
            val difference = DiffUtil.calculateDiff(diffUtil)
            list.clear()
            list.addAll(data)
            difference.dispatchUpdatesTo(this)
        }
    }

    fun appendNewData(search: List<SearchDetailItemResult>?) {

        if (search != null) {
            for (item in search) {
                list.add(item)
                notifyItemInserted(list.size - 1)

            }
        }
    }

    fun addLoader() {

        list.add(SearchDetailItemResult(imdbID = LOADER_IMDB_ID, uniqueId = 0))
        notifyItemInserted(list.size - 1)

    }

    fun removeLoader() {
        list.removeAt(list.size - 1)
        notifyItemRemoved(list.size);
    }

    fun isFav(position: Int): Boolean {

        return list[position].isFav
    }

    fun clearAndNotify() {
        list.clear()
        notifyDataSetChanged()
    }


}

class ProgressViewHolder(view: View?) :
    RecyclerView.ViewHolder(view!!) {

}

class MovieViewHolder(
    private val view: View,
    private val onItemClickListener: onItemClickListener
) : RecyclerView.ViewHolder(view),
    View.OnClickListener {

    val movieImg: ImageView = view.findViewById(R.id.movie_img)
    val movieTitle: TextView = view.findViewById(R.id.movie_title)
    val movieyear: TextView = view.findViewById(R.id.movie_year)
    val moviefav: ImageView = view.findViewById(R.id.movie_fav_icon)
    var list = ArrayList<SearchDetailItemResult>()

    init {
        view.setOnClickListener(this)
    }

    fun bindUI(position: Int, list: ArrayList<SearchDetailItemResult>) {
        this.list = list
        if (!TextUtils.isEmpty(list[position].Title)) {
            movieTitle.text = list[position].Title;
        } else {
            movieTitle.text = ""
        }
        if (!TextUtils.isEmpty(list[position].Year)) {
            movieyear.text = list[position].Year;
        } else {
            movieyear.text = ""
        }

        if (list[position].isFav) {
            moviefav.setImageResource(R.drawable.fav_icon)
        } else {
            moviefav.setImageResource(R.drawable.unfav_icon)
        }

        val requestOptions = RequestOptions();
        requestOptions.placeholder(R.drawable.ic_baseline_movie_24);


        Glide.with(view.context)
            .setDefaultRequestOptions(requestOptions)
            .load(list[position].Poster).into(movieImg)

    }

    override fun onClick(p0: View?) {
        list[adapterPosition].imdbID?.let { onItemClickListener.onItemClicked(adapterPosition, it) }
    }


}

interface onItemClickListener {
    fun onItemClicked(position: Int, imdbID: String)

}